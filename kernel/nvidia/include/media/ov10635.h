/*
 * ov10635.h - ov10635 sensor driver
 *
 * Copyright (c) 2016 NVIDIA Corporation.  All rights reserved.
 *
 * This file is licensed under the terms of the GNU General Public License
 * version 2. This program is licensed "as is" without any warranty of any
 * kind, whether express or implied.
 */

#ifndef __OV10635_H__
#define __OV10635_H__

#include <linux/ioctl.h>

struct ov10635_sensordata {
	__u32 fuse_id_size;
	__u8 fuse_id[16];
};

struct ov10635_mode {
	__u32 xres;
	__u32 yres;
	__u32 fps;
	__u32 frame_length;
	__u32 coarse_time;
	__u16 gain;
	__u8 fsync_master;
};

struct ov10635_grouphold {
	__u32	frame_length;
	__u8	frame_length_enable;
	__u32	coarse_time;
	__u8	coarse_time_enable;
	__s32	gain;
	__u8	gain_enable;
};

struct ov10635_flash_control {
	u8 enable;
	u8 edge_trig_en;
	u8 start_edge;
	u8 repeat;
	u16 delay_frm;
};

#ifdef __KERNEL__
struct ov10635_power_rail {
	struct regulator *dvdd;
	struct regulator *avdd;
	struct regulator *iovdd;
	struct regulator *vif;
};

struct ov10635_platform_data {
	struct ov10635_flash_control flash_cap;
	const char *dev_name;
	unsigned num;
	int reset_gpio;
	int cam1sid_gpio;
	int cam2sid_gpio;
	int cam3sid_gpio;
	unsigned default_i2c_sid_low;
	unsigned default_i2c_sid_high;
	unsigned regw_sid_low;
	unsigned regw_sid_high;
	unsigned cam1i2c_addr;
	unsigned cam2i2c_addr;
	unsigned cam3i2c_addr;
	bool cam_use_26mhz_mclk;
	bool cam_change_i2c_addr;
	bool cam_i2c_recovery;
	bool cam_sids_high_to_low;
	bool cam_skip_sw_reset;
	bool cam_use_osc_for_mclk;
	int (*power_on)(struct ov10635_power_rail *pw);
	int (*power_off)(struct ov10635_power_rail *pw);
	const char *mclk_name;
};
#endif /* __KERNEL__ */

#endif /* __OV10635_H__ */
