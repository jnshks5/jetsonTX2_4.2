/*
 * ov10635.c - ov10635 sensor driver
 *
 * Copyright (c) 2016, NVIDIA CORPORATION, All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <media/camera_common.h>

#ifndef __OV10635_I2C_TABLES__
#define __OV10635_I2C_TABLES__

#define OV10635_TABLE_WAIT_MS	100
#define OV10635_TABLE_END	1
#define OV10635_MAX_RETRIES	10
#define OV10635_WAIT_MS	10
#define OV10635_ADJUST_HS_PREPARE 1

#define ov10635_reg struct reg_8

enum {
	OV10635_MODE_1280x720,
	OV10635_MODE_INVALID
};

static const int ov10635_30fps[] = {
	30,
};

static const struct camera_common_frmfmt ov10635_frmfmt[] = {
	{{1280, 720}, ov10635_30fps, 1, 0, OV10635_MODE_1280x720},
};
#endif  /* __OV10635_I2C_TABLES__ */
