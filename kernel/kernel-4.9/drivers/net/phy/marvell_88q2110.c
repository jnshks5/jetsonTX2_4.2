/*
 * @file marvell_88q2210.c
 * @brief Driver for Marvell 88Q2210 PHY
 * @author Toan Vo <toanvo@adasone.com>
 * @refs 88Q211x_Z1_A0_A1_A2_API_V1_20
 * MV-S400460-00---88Q211x_Software_Initialization_Guide.pdf
 * 01_MV-S110850-00F datasheet.pdf
 */

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/errno.h>
#include <linux/unistd.h>
#include <linux/interrupt.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/skbuff.h>
#include <linux/spinlock.h>
#include <linux/mm.h>
#include <linux/module.h>
#include <linux/mii.h>
#include <linux/ethtool.h>
#include <linux/phy.h>
#include <linux/marvell_phy.h>
#include <linux/of.h>
#include <linux/iopoll.h>

//#define TEST_PRINT(fmt, ...) pr_warn("(%s)<%d> : " fmt, __func__, __LINE__, ##__VA_ARGS__)
#define TEST_PRINT(fmt, ...)

MODULE_DESCRIPTION("Marvell PHY 88Q2110 driver");
MODULE_AUTHOR("Toan Vo <toanvo@adasone.com>");
MODULE_LICENSE("GPL");

#if 1 // 88Q211x_Z1_A0_A1_A2_API_V1_20
// 1000 BASE-T1 Operating Mode
#define MRVL_88Q2112_MODE_LEGACY    0x06B0
#define MRVL_88Q2112_MODE_DEFAULT   0x0000
#define MRVL_88Q2112_MODE_MSK       0x07F0
#define MRVL_88Q2112_MODE_ADVERTISE 0x0002
#define MRVL_88Q2112_MODE_ERROR     0xFFFF

// PHY Revisions
#define MRVL_88Q2112_A2     0x0003
#define MRVL_88Q2112_A1     0x0002
#define MRVL_88Q2112_A0     0x0001
#define MRVL_88Q2112_Z1     0x0000

// Current Speed
#define MRVL_88Q2112_1000BASE_T1    0x0001
#define MRVL_88Q2112_100BASE_T1     0x0000

// Auto-Negotiation Controls - reg 7.0x0200
#define MRVL_88Q2112_AN_DISABLE     0x0000
#define MRVL_88Q2112_AN_RESET       0x8000
#define MRVL_88Q2112_AN_ENABLE      0x1000
#define MRVL_88Q2112_AN_RESTART     0x0200

// Auto-Negotiation Status - reg 7.0x0201
#define MRVL_88Q2112_AN_COMPLETE    0x0020

// Auto-Negotiation Flags - reg 7.0x0203
#define MRVL_88Q2112_AN_FE_ABILITY      0x0020
#define MRVL_88Q2112_AN_GE_ABILITY      0x0080
#define MRVL_88Q2112_AN_PREFER_MASTER   0x0010

static bool applyOperateModeConfig ( struct phy_device *phydev );
static uint16_t loadOperateModeSetting(struct phy_device *phydev);
static bool saveOperateModeSetting(struct phy_device *phydev, uint16_t modeId);
static bool setAneg(struct phy_device *phydev, uint16_t forceMaster, uint16_t flags);

// Read out a 16-bit value from a register through SMI interface.
// @param phydev The PHY device bus
// @param devAddr section of register map (Ex: 0x07 : Auto-Neg registers)
// @param regAddr register address within section
// @return value in register if successful
static inline uint16_t regRead(
    struct phy_device *phydev,
    uint16_t devAddr, uint16_t regAddr)
{
    //return phy_read_mmd_indirect(phydev, regAddr, devAddr);
    int ret = phy_read_mmd_indirect(phydev, regAddr, devAddr);
    return ret;
}

// Write a 16-bit value into a register through SMI interface.
// @param phydev The PHY device bus
// @param devAddr section of register map (Ex: 0x07 : Auto-Neg registers)
// @param regAddr register address within section
// @param data 16-bit to write into register
// @return void
static inline void regWrite(
    struct phy_device *phydev,
    uint16_t devAddr, uint16_t regAddr, uint16_t data)
{
    return phy_write_mmd_indirect(phydev, regAddr, devAddr, data);
}

// Obtain 88Q211x model number from a register. Useful to verify SMI connection.
// @param phydev The PHY device bus
// @return constant value MRVL_88Q2112_PRODUCT_ID read from a register
static inline uint16_t getModelNum ( struct phy_device *phydev )
{
    uint16_t modelNum = regRead(phydev, 1, 0x0003);
    return ( (modelNum & 0x03F0) >> 4 );
}

// Obtain 88Q211x revision number from a register.
// @param phydev The PHY device bus
// @return PHY revision MRVL_88Q2112_XX
static uint16_t getRevNum ( struct phy_device *phydev )
{
    uint16_t revision = regRead(phydev, 1, 0x0003);
    return revision & 0x000F;
}

// Set Master/Slave mode of the PHY by software
// @param phydev The PHY device bus
// @param forceMaster   non-zero (Master)   otherwise (Slave)
// @return void
static void setMasterSlave ( struct phy_device *phydev, uint16_t forceMaster )
{
    uint16_t regData = regRead(phydev, 1, 0x0834);
    TEST_PRINT("reg 834: %x\n", regData);
    if (0x0 != forceMaster)
        regData |= 0x4000;
    else
        regData &= 0xBFFF;
    regWrite(phydev, 1, 0x0834, regData);
}

// Get current master/slave setting
// @param phydev The PHY device bus
// @return 0x1 if master, 0x0 if slave
// static uint16_t getMasterSlave ( struct phy_device *phydev )
// {
// 	return ( (regRead(phydev, 7, 0x8001) >> 14) & 0x0001 );
// }

// See if Auto-Negotiation is enabled
// @param phydev The PHY device bus
// @return true if enabled, false otherwise
static bool getAnegEnabled ( struct phy_device *phydev ) {
    return ( 0x0 != (regRead(phydev, 7, 0x0200) & MRVL_88Q2112_AN_ENABLE) );
}

// Get current data rate – negotiated or set directly
// @param phydev The PHY device bus
// @return 0x1 if 1000 BASE-T1, 0x0 if 100 BASE-T1
static uint16_t getSpeed ( struct phy_device *phydev )
{
    if (getAnegEnabled(phydev))
        return ( (regRead(phydev, 7, 0x801a) & 0x4000) >> 14 );
    else
        return (regRead(phydev, 1, 0x0834) & 0x0001);
}

// ================================================
// Code from internal shared configuration functions
// - _applyQ2112GeSetting: Speed 1000 related configuration
// - _applyQ2112FeSetting: Speed 100 related configuration
// - _applyQ2112AnegSetting: ANEG related configuration
// - _applyQ2112GeSoftReset: Reset for 1000 rate
// - _applyQ2112FeSoftReset: Reset for 100 rate
// ================================================

static void _applyQ2112GeSetting ( struct phy_device *phydev, uint16_t isAneg ) {
    uint16_t regData = 0;

    mdelay(2);
    if (0x0 != isAneg)
        regWrite(phydev, 7, 0x0200, MRVL_88Q2112_AN_ENABLE | MRVL_88Q2112_AN_RESTART);
    else
        regWrite(phydev, 7, 0x0200, MRVL_88Q2112_AN_DISABLE);

    switch (getRevNum(phydev)) {
    case MRVL_88Q2112_A2:       // fall-through to MRVL_88Q2112_A1 intentional
    case MRVL_88Q2112_A1:       // fall-through to MRVL_88Q2112_A0 intentional
    case MRVL_88Q2112_A0:
        regWrite(phydev, 1, 0x0900, 0x4000);

        regData = regRead(phydev, 1, 0x0834);
        regData = (regData & 0xFFF0) | 0x0001;
        regWrite(phydev, 1, 0x0834, regData);

        regWrite(phydev, 3, 0xFFE4, 0x07B5);
        regWrite(phydev, 3, 0xFFE4, 0x06B6);
        mdelay(5);

        regWrite(phydev, 3, 0xFFDE, 0x402F);
        regWrite(phydev, 3, 0xFE2A, 0x3C3D);
        regWrite(phydev, 3, 0xFE34, 0x4040);
        regWrite(phydev, 3, 0xFE4B, 0x9337);
        regWrite(phydev, 3, 0xFE2A, 0x3C1D);
        regWrite(phydev, 3, 0xFE34, 0x0040);
        regWrite(phydev, 7, 0x8032, 0x0064);
        regWrite(phydev, 7, 0x8031, 0x0A01);
        regWrite(phydev, 7, 0x8031, 0x0C01);
        regWrite(phydev, 3, 0xFE0F, 0x0000);
        regWrite(phydev, 3, 0x800C, 0x0000);
        regWrite(phydev, 3, 0x801D, 0x0800);
        regWrite(phydev, 3, 0xFC00, 0x01C0);
        regWrite(phydev, 3, 0xFC17, 0x0425);
        regWrite(phydev, 3, 0xFC94, 0x5470);
        regWrite(phydev, 3, 0xFC95, 0x0055);
        regWrite(phydev, 3, 0xFC19, 0x08D8);
        regWrite(phydev, 3, 0xFC1a, 0x0110);
        regWrite(phydev, 3, 0xFC1b, 0x0A10);
        regWrite(phydev, 3, 0xFC3A, 0x2725);
        regWrite(phydev, 3, 0xFC61, 0x2627);
        regWrite(phydev, 3, 0xFC3B, 0x1612);
        regWrite(phydev, 3, 0xFC62, 0x1C12);
        regWrite(phydev, 3, 0xFC9D, 0x6367);
        regWrite(phydev, 3, 0xFC9E, 0x8060);
        regWrite(phydev, 3, 0xFC00, 0x01C8);
        regWrite(phydev, 3, 0x8000, 0x0000);
        regWrite(phydev, 3, 0x8016, 0x0011);

        if (MRVL_88Q2112_A0 != getRevNum(phydev))
            regWrite(phydev, 3, 0xFDA3, 0x1800);

        regWrite(phydev, 3, 0xFE02, 0x00C0);
        regWrite(phydev, 3, 0xFFDB, 0x0010);
        regWrite(phydev, 3, 0xFFF3, 0x0020);
        regWrite(phydev, 3, 0xFE40, 0x00A6);

        regWrite(phydev, 3, 0xFE60, 0x0000);
        regWrite(phydev, 3, 0xFE04, 0x0008);
        regWrite(phydev, 3, 0xFE2A, 0x3C3D);
        regWrite(phydev, 3, 0xFE4B, 0x9334);

        regWrite(phydev, 3, 0xFC10, 0xF600);
        regWrite(phydev, 3, 0xFC11, 0x073D);
        regWrite(phydev, 3, 0xFC12, 0x000D);
        regWrite(phydev, 3, 0xFC13, 0x0010);
        break;

    default:    // Z1 case
        // port init.
        regWrite(phydev, 3, 0x0000, 0x0000);
        regWrite(phydev, 1, 0x0900, 0x0000);
        regWrite(phydev, 3, 0x800d, 0x0000);
        // Link LED
        regWrite(phydev, 3, 0x8000, 0x0000);
        regWrite(phydev, 3, 0x8016, 0x0011);
        // restore default from 100M
        regWrite(phydev, 3, 0xFE05, 0x0000);
        regWrite(phydev, 3, 0xFE07, 0x6A10);
        regWrite(phydev, 3, 0xFB95, 0x5720);
        regWrite(phydev, 3, 0xFE5D, 0x175C);
        regWrite(phydev, 3, 0x8016, 0x0071);
        //set speed
        regData = regRead(phydev, 1, 0x0834);
        regData = (regData & 0xFFF0) | 0x0001;
        regWrite(phydev, 1, 0x0834, regData);
        mdelay(500);
        // Init code
        regWrite(phydev, 3, 0xFE12, 0x000E);
        regWrite(phydev, 3, 0xFE05, 0x05AA);
        regWrite(phydev, 3, 0xFE04, 0x0016);
        regWrite(phydev, 3, 0xFE07, 0x681F);
        regWrite(phydev, 3, 0xFE5D, 0x045C);
        regWrite(phydev, 3, 0xFE7C, 0x001E);
        regWrite(phydev, 3, 0xFC00, 0x01C0);
        regWrite(phydev, 7, 0x8032, 0x0020);
        regWrite(phydev, 7, 0x8031, 0x0012);
        regWrite(phydev, 7, 0x8031, 0x0A12);
        regWrite(phydev, 7, 0x8032, 0x003C);
        regWrite(phydev, 7, 0x8031, 0x0001);
        regWrite(phydev, 7, 0x8031, 0x0A01);
        regWrite(phydev, 3, 0xFC10, 0xD870);
        regWrite(phydev, 3, 0xFC11, 0x1522);
        regWrite(phydev, 3, 0xFC12, 0x07FA);
        regWrite(phydev, 3, 0xFC13, 0x010B);
        regWrite(phydev, 3, 0xFC15, 0x35A4);
        regWrite(phydev, 3, 0xFC2D, 0x3C34);
        regWrite(phydev, 3, 0xFC2E, 0x104B);
        regWrite(phydev, 3, 0xFC2F, 0x1C15);
        regWrite(phydev, 3, 0xFC30, 0x3C3C);
        regWrite(phydev, 3, 0xFC31, 0x3C3C);
        regWrite(phydev, 3, 0xFC3A, 0x2A2A);
        regWrite(phydev, 3, 0xFC61, 0x2829);
        regWrite(phydev, 3, 0xFC3B, 0x0E0E);
        regWrite(phydev, 3, 0xFC62, 0x1C12);
        regWrite(phydev, 3, 0xFC32, 0x03D2);
        regWrite(phydev, 3, 0xFC46, 0x0200);
        regWrite(phydev, 3, 0xFC86, 0x0401);
        regWrite(phydev, 3, 0xFC4E, 0x1820);
        regWrite(phydev, 3, 0xFC9C, 0x0101);
        regWrite(phydev, 3, 0xFC95, 0x007A);
        regWrite(phydev, 3, 0xFC3E, 0x221F);
        regWrite(phydev, 3, 0xFC3F, 0x0A08);
        regWrite(phydev, 3, 0xFC03, 0x020E);
        regWrite(phydev, 3, 0xFC04, 0x0077);
        regWrite(phydev, 3, 0xFC03, 0x0210);
        regWrite(phydev, 3, 0xFC04, 0x0088);
        regWrite(phydev, 3, 0xFC03, 0x0215);
        regWrite(phydev, 3, 0xFC04, 0x00AA);
        regWrite(phydev, 3, 0xFC03, 0x01D5);
        regWrite(phydev, 3, 0xFC04, 0x00AA);
        regWrite(phydev, 3, 0xFC03, 0x0216);
        regWrite(phydev, 3, 0xFC04, 0x00AB);
        regWrite(phydev, 3, 0xFC03, 0x01D6);
        regWrite(phydev, 3, 0xFC04, 0x00AB);
        regWrite(phydev, 3, 0xFC03, 0x0213);
        regWrite(phydev, 3, 0xFC04, 0x00A0);
        regWrite(phydev, 3, 0xFC03, 0x01D3);
        regWrite(phydev, 3, 0xFC04, 0x00A0);
        regWrite(phydev, 3, 0xFC03, 0x0214);
        regWrite(phydev, 3, 0xFC04, 0x00AB);
        regWrite(phydev, 3, 0xFC03, 0x01D4);
        regWrite(phydev, 3, 0xFC04, 0x00AB);
        regWrite(phydev, 3, 0xFC03, 0x046B);
        regWrite(phydev, 3, 0xFC04, 0x00FA);
        regWrite(phydev, 3, 0xFC03, 0x046C);
        regWrite(phydev, 3, 0xFC04, 0x01F4);
        regWrite(phydev, 3, 0xFC03, 0x046E);
        regWrite(phydev, 3, 0xFC04, 0x01F4);
        regWrite(phydev, 3, 0xFC03, 0x0455);
        regWrite(phydev, 3, 0xFC04, 0x0320);
        regWrite(phydev, 3, 0xFC03, 0x0416);
        regWrite(phydev, 3, 0xFC04, 0x0323);
        regWrite(phydev, 3, 0xFC03, 0x0004);
        regWrite(phydev, 3, 0xFC04, 0x0000);
        regWrite(phydev, 3, 0xFC03, 0x03CC);
        regWrite(phydev, 3, 0xFC04, 0x0055);
        regWrite(phydev, 3, 0xFC03, 0x03CD);
        regWrite(phydev, 3, 0xFC04, 0x0055);
        regWrite(phydev, 3, 0xFC03, 0x03CE);
        regWrite(phydev, 3, 0xFC04, 0x0022);
        regWrite(phydev, 3, 0xFC03, 0x03CF);
        regWrite(phydev, 3, 0xFC04, 0x0022);
        regWrite(phydev, 3, 0xFC03, 0x03D0);
        regWrite(phydev, 3, 0xFC04, 0x0022);
        regWrite(phydev, 3, 0xFC03, 0x03D1);
        regWrite(phydev, 3, 0xFC04, 0x0022);
        regWrite(phydev, 3, 0xFC03, 0x03E4);
        regWrite(phydev, 3, 0xFC04, 0x0055);
        regWrite(phydev, 3, 0xFC03, 0x03E5);
        regWrite(phydev, 3, 0xFC04, 0x0055);
        regWrite(phydev, 3, 0xFC03, 0x03E6);
        regWrite(phydev, 3, 0xFC04, 0x0022);
        regWrite(phydev, 3, 0xFC03, 0x03E7);
        regWrite(phydev, 3, 0xFC04, 0x0022);
        regWrite(phydev, 3, 0xFC03, 0x03E8);
        regWrite(phydev, 3, 0xFC04, 0x0022);
        regWrite(phydev, 3, 0xFC03, 0x03E9);
        regWrite(phydev, 3, 0xFC04, 0x0022);
        regWrite(phydev, 3, 0xFC03, 0x040C);
        regWrite(phydev, 3, 0xFC04, 0x0033);
        regWrite(phydev, 3, 0xFC5D, 0x06BF);
        regWrite(phydev, 3, 0xFC89, 0x0003);
        regWrite(phydev, 3, 0xFC5C, 0x007F);
        regWrite(phydev, 3, 0xFC69, 0x383A);
        regWrite(phydev, 3, 0xFC6A, 0x383A);
        regWrite(phydev, 3, 0xFC6B, 0x0082);
        regWrite(phydev, 3, 0xFC6F, 0x888F);
        regWrite(phydev, 3, 0xFC70, 0x0D1A);
        regWrite(phydev, 3, 0xFC71, 0x0505);
        regWrite(phydev, 3, 0xFC72, 0x090C);
        regWrite(phydev, 3, 0xFC73, 0x0C0F);
        regWrite(phydev, 3, 0xFC74, 0x0400);
        regWrite(phydev, 3, 0xFC75, 0x0103);
        regWrite(phydev, 3, 0xFC7A, 0x081E);
        regWrite(phydev, 3, 0xFC8C, 0xBC40);
        regWrite(phydev, 3, 0xFC8D, 0x9830);
        regWrite(phydev, 3, 0xFC91, 0x0000);
        regWrite(phydev, 3, 0xFC63, 0x4440);
        regWrite(phydev, 3, 0xFC64, 0x3C3F);
        regWrite(phydev, 3, 0xFC65, 0x783C);
        regWrite(phydev, 3, 0xFC66, 0x0002);
        regWrite(phydev, 3, 0xFC7B, 0x7818);
        regWrite(phydev, 3, 0xFC7C, 0xC440);
        regWrite(phydev, 3, 0xFC7D, 0x5360);
        regWrite(phydev, 3, 0xFC5F, 0x4034);
        regWrite(phydev, 3, 0xFC60, 0x7858);
        regWrite(phydev, 3, 0xFC7E, 0x003F);
        regWrite(phydev, 3, 0xFC8E, 0x0003);
        regWrite(phydev, 3, 0xFC57, 0x1820);
        regWrite(phydev, 3, 0xFC00, 0x01C8);
        regWrite(phydev, 3, 0xFC93, 0x141C);
        regWrite(phydev, 3, 0xFC9B, 0x0091);
        regWrite(phydev, 3, 0xFC94, 0x6D88);
        regWrite(phydev, 3, 0xFE4A, 0x5653);
        regWrite(phydev, 3, 0x0900, 0x8000);
        break;
    }
}

static void _applyQ2112FeSetting(struct phy_device *phydev, uint16_t isAneg) {
    uint16_t regData = 0;

    mdelay(1);
    if (0x0 != isAneg)
        regWrite(phydev, 7, 0x0200, MRVL_88Q2112_AN_ENABLE | MRVL_88Q2112_AN_RESTART);
    else
        regWrite(phydev, 7, 0x0200, MRVL_88Q2112_AN_DISABLE);

    if (MRVL_88Q2112_Z1 != getRevNum(phydev)) {    // A2/A1/A0
        regWrite(phydev, 3, 0xFA07, 0x0202);

        regData = regRead(phydev, 1, 0x0834);
        regData = regData & 0xFFF0;
        regWrite(phydev, 1, 0x0834, regData);
        mdelay(5);

        regWrite(phydev, 3, 0x8000, 0x0000);
        regWrite(phydev, 3, 0x8100, 0x0200);
        regWrite(phydev, 3, 0xFA1E, 0x0002);
        regWrite(phydev, 3, 0xFE5C, 0x2402);
        regWrite(phydev, 3, 0xFA12, 0x001F);
        regWrite(phydev, 3, 0xFA0C, 0x9E05);
        regWrite(phydev, 3, 0xFBDD, 0x6862);
        regWrite(phydev, 3, 0xFBDE, 0x736E);
        regWrite(phydev, 3, 0xFBDF, 0x7F79);
        regWrite(phydev, 3, 0xFBE0, 0x8A85);
        regWrite(phydev, 3, 0xFBE1, 0x9790);
        regWrite(phydev, 3, 0xFBE3, 0xA39D);
        regWrite(phydev, 3, 0xFBE4, 0xB0AA);
        regWrite(phydev, 3, 0xFBE5, 0x00B8);
        regWrite(phydev, 3, 0xFBFD, 0x0D0A);
        regWrite(phydev, 3, 0xFBFE, 0x0906);
        regWrite(phydev, 3, 0x801D, 0x8000);
        regWrite(phydev, 3, 0x8016, 0x0011);
    }
    else {  // Z1 Case
        // port init.
        regWrite(phydev, 3, 0x0000, 0x0000);
        regWrite(phydev, 1, 0x0900, 0x0000);
        regWrite(phydev, 3, 0x800D, 0x0000);
        // Link LED
        regWrite(phydev, 3, 0x8000, 0x0000);
        regWrite(phydev, 3, 0x8016, 0x0011);
        //set speed
        regData = regRead(phydev, 1, 0x0834);
        regData = regData & 0xFFF0;
        regWrite(phydev, 1, 0x0834, regData);
        mdelay(500);
        // Init code
        regWrite(phydev, 3, 0x8000, 0x0000);
        regWrite(phydev, 3, 0xFE05, 0x3DAA);
        regWrite(phydev, 3, 0xFE07, 0x6BFF);
        regWrite(phydev, 3, 0xFB95, 0x52F0);
        regWrite(phydev, 3, 0xFE5D, 0x171C);
        regWrite(phydev, 3, 0x8016, 0x0011);
        regWrite(phydev, 3, 0x0900, 0x8000);
    }
}

static void _applyQ2112AnegSetting(struct phy_device *phydev) {
    uint16_t regDataAuto = 0;
    if (MRVL_88Q2112_Z1 != getRevNum(phydev)) {

        regDataAuto = regRead(phydev, 7, 0x8032);
        regWrite(phydev, 7, 0x8032, regDataAuto | 0x0001);
        regWrite(phydev, 7, 0x8031, 0x0013);
        regWrite(phydev, 7, 0x8031, 0x0a13);

        regDataAuto = regRead(phydev, 7, 0x8032);
        regDataAuto = (0xFC00 & regDataAuto) | 0x0012;
        regWrite(phydev, 7, 0x8032, regDataAuto);
        regWrite(phydev, 7, 0x8031, 0x0016);
        regWrite(phydev, 7, 0x8031, 0x0a16);

        regDataAuto = regRead(phydev, 7, 0x8032);
        regDataAuto = (0xFC00 & regDataAuto) | 0x64;
        regWrite(phydev, 7, 0x8032, regDataAuto);
        regWrite(phydev, 7, 0x8031, 0x0017);
        regWrite(phydev, 7, 0x8031, 0x0a17);

        regWrite(phydev, 3, 0x800C, 0x0008);
        regWrite(phydev, 3, 0xFE04, 0x0016);
    }
    else {  // Z1 Case
        regWrite(phydev, 7, 0x8032, 0x0200);
        regWrite(phydev, 7, 0x8031, 0x0a03);
    }
}

static void _applyQ2112GeSoftReset(struct phy_device *phydev) {
    uint16_t regDataAuto = 0;

    if (MRVL_88Q2112_Z1 != getRevNum(phydev)) {    // A2/A1/A0
        if (getAnegEnabled(phydev)) {
            regWrite(phydev, 3, 0xFFF3, 0x0024);
        }
        //enable low-power mode
        regDataAuto = regRead(phydev, 1, 0x0000);
        regWrite(phydev, 1, 0x0000, regDataAuto | 0x0800);

        regWrite(phydev, 3, 0xFFF3, 0x0020);
        regWrite(phydev, 3, 0xFFE4, 0x000C);
        mdelay(1);

        regWrite(phydev, 3, 0xffe4, 0x06B6);

        // disable low-power mode
        regWrite(phydev, 1, 0x0000, regDataAuto & 0xF7FF);
        mdelay(1);

        regWrite(phydev, 3, 0xFC47, 0x0030);
        regWrite(phydev, 3, 0xFC47, 0x0031);
        regWrite(phydev, 3, 0xFC47, 0x0030);
        regWrite(phydev, 3, 0xFC47, 0x0000);
        regWrite(phydev, 3, 0xFC47, 0x0001);
        regWrite(phydev, 3, 0xFC47, 0x0000);

        regWrite(phydev, 3, 0x0900, 0x8000);

        regWrite(phydev, 1, 0x0900, 0x0000);
        regWrite(phydev, 3, 0xFFE4, 0x000C);
    }
    else {  // Z1 Case
        regDataAuto = regRead(phydev, 3, 0x0900);
        regWrite(phydev, 3, 0x0900, regDataAuto | 0x8000);
        mdelay(5);
    }
}

static void _applyQ2112FeSoftReset(struct phy_device *phydev) {
    uint16_t regData = 0;
    if (MRVL_88Q2112_Z1 != getRevNum(phydev)) {    // A2/A1/A0
        regWrite(phydev, 3, 0x0900, 0x8000);
        regWrite(phydev, 3, 0xFA07, 0x0200);
    }
    else {    // Z1 Case
        regData = regRead(phydev, 3, 0x0900);
        regWrite(phydev, 3, 0x0900, regData | 0x8000);
        mdelay(5);
    }
}
// ================================================
// End of code from internal shared configuration functions
// ================================================

// Initialize for 100 BASE-T1
// @param phydev The PHY device bus
// @return void
static void initQ2112Fe ( struct phy_device *phydev ) {
    _applyQ2112FeSetting(phydev, MRVL_88Q2112_AN_DISABLE);
    _applyQ2112FeSoftReset(phydev);
}

// Initialize for 1000 BASE-T1
// @param phydev The PHY device bus
// @return true if PHY initialized successfully, false otherwise
static bool initQ2112Ge ( struct phy_device *phydev ) {
    _applyQ2112GeSetting(phydev, MRVL_88Q2112_AN_DISABLE);
    if (!applyOperateModeConfig(phydev))
        return false;
    _applyQ2112GeSoftReset(phydev);
    return true;
}

// Get link status including PCS (local/remote) and local PMA to see if it's ready to send traffic
// @param phydev The PHY device bus
// @return true if link is up, false otherwise
static bool checkLink ( struct phy_device *phydev )
{
    uint16_t retData1, retData2 = 0;
    if (MRVL_88Q2112_1000BASE_T1 == getSpeed(phydev)) {
        retData1 = regRead(phydev, 3, 0x0901);
        retData1 = regRead(phydev, 3, 0x0901);    // Read twice: register latches low value
        retData2 = regRead(phydev, 7, 0x8001);
    }
    else {
        retData1 = regRead(phydev, 3, 0x8109);
        retData2 = regRead(phydev, 3, 0x8108);
    }
    mdelay(1);
    return (0x0 != (retData1 & 0x0004)) && (0x0 != (retData2 & 0x3000));
}

// Save initial operating mode for the A2 PHY in memory
// @param phydev The PHY device bus
// @param selfMode operating mode MRVL_88Q2112_MODE_XXXX   (see 3.1 Definitions)
// @return true if successfully saved the mode, false on error
static bool manualSetOperateModeSetting ( struct phy_device *phydev, uint16_t selfMode )
{
    if (MRVL_88Q2112_A2 == getRevNum(phydev))
        return saveOperateModeSetting(phydev, selfMode & MRVL_88Q2112_MODE_MSK);
    else
        return saveOperateModeSetting(phydev, MRVL_88Q2112_MODE_LEGACY);
}

// Fetch current operating mode from memory and apply in PHY
// @param phydev The PHY device bus
// @return true if successfully applied the mode, false on error
static bool applyOperateModeConfig ( struct phy_device *phydev ) {
    uint16_t opMode = 0;
    bool result = false;
    switch (getRevNum(phydev)) {

    case MRVL_88Q2112_A1:
        regWrite(phydev, 3, 0xFDB8, 0x0001);    //Set A1 to legacy mode
        regWrite(phydev, 1, 0x0902, MRVL_88Q2112_MODE_LEGACY | MRVL_88Q2112_MODE_ADVERTISE);
        result = true;
        break;

    case MRVL_88Q2112_A0:   // fall-through to MRVL_88Q2112_Z1 intentional
    case MRVL_88Q2112_Z1:
        regWrite(phydev, 1, 0x0902, MRVL_88Q2112_MODE_LEGACY | MRVL_88Q2112_MODE_ADVERTISE);
        result = true;
        break;

    case MRVL_88Q2112_A2:
        opMode = loadOperateModeSetting(phydev);
        if (MRVL_88Q2112_MODE_ERROR != opMode) {
            if (MRVL_88Q2112_MODE_LEGACY == opMode) {
                // Enable 1000 BASE-T1 legacy mode support
                regWrite(phydev, 3, 0xFDB8, 0x0001);
                regWrite(phydev, 3, 0xFD3D, 0x0C14);
            }
            else {
                // Set back to default compliant mode setting
                regWrite(phydev, 3, 0xFDB8, 0x0000);
                regWrite(phydev, 3, 0xFD3D, 0x0000);
            }
            regWrite(phydev, 1, 0x0902, opMode | MRVL_88Q2112_MODE_ADVERTISE);
            result = true;
        }
        // no else statement
        break;

    default:    // error case - unexpected revision
        break;
    }

    return result;
}

#define MRVL_88Q2112_PRODUCT_ID         0x0018

// PHY Init Return flags
#define MRVL_88Q2112_INIT_DONE              0x0001
#define MRVL_88Q2112_INIT_ERROR_ARGS        0x0100
#define MRVL_88Q2112_INIT_ERROR_SMI         0x0200
#define MRVL_88Q2112_INIT_ERROR_SAVE_MODE   0x0400
#define MRVL_88Q2112_INIT_ERROR_INIT_GE     0x0800
#define MRVL_88Q2112_INIT_ERROR_FAILED      0x1000

// PHY Init (and InitAN) Flag Arguments
#define MRVL_88Q2112_INIT_MASTER        0x0001
#define MRVL_88Q2112_INIT_AN_ENABLE     0x0002
#define MRVL_88Q2112_INIT_FE            0x0004
#define MRVL_88Q2112_INIT_GE            0x0008

// Link-Up Timeout
#define MRVL_88Q2112_LINKUP_TIMEOUT     200

static bool saveOperateModeSetting(struct phy_device *phydev, uint16_t modeId)
{
    return true;
}

static uint16_t loadOperateModeSetting(struct phy_device *phydev)
{
    return MRVL_88Q2112_MODE_DEFAULT;
}

// See if Link Partner signalled LEGACY mode during Training stage of 1000 BASE-T1.
// During PAM2 training stage, PHYs can exchange PHY Revision information.
// 88Q2112 revisions Z1/A0/A1 will signal MRVL_88Q2112_MODE_LEGACY if Init sequence is executed using API V1.10 or later version.
// This method is used with A2 only, as Z1/A0/A1 are always in LEGACY mode.
// @param phydev The PHY device bus
// @return true if Link Partner in LEGACY mode and speed is 1000 BASE-T1, false otherwise.
static bool isLinkPartnerInLegacyMode ( struct phy_device *phydev ) {
    if (MRVL_88Q2112_100BASE_T1 == getSpeed(phydev))
        return false;
    if (MRVL_88Q2112_A2 == getRevNum(phydev))
        if (MRVL_88Q2112_MODE_LEGACY == (regRead(phydev, 1, 0x0903) & MRVL_88Q2112_MODE_MSK))
            return true;
    // If not A2 and did not receive LEGACY register indicators from Link Partner, return false
    return false;
}

// Initialize PHY in 100/1000 BASE-T1 mode WITHOUT Auto-Neg.
// @param phydev The PHY device bus
// @param flags bitwise flags to set PHY parameters:
//        MRVL_88Q2112_INIT_MASTER
//        MRVL_88Q2112_INIT_FE
//        MRVL_88Q2112_INIT_GE
// @param addedTimeMarginMS system-dependent extra margin until link-up in milliseconds
// @return see MRVL_88Q2112_INIT_#### return codes
static uint16_t phyInit ( struct phy_device *phydev, uint16_t flags, uint16_t addedTimeMarginMS )
{
    // 1) Ensure functional communication to PHY SMI
    int value = 0;
    int ret = 0;
    pr_warn("forcing operation mode\n");
    ret = readx_poll_timeout(getModelNum, phydev, value,
                             MRVL_88Q2112_PRODUCT_ID == value, 1000, 100000);
    if (ret)
        return MRVL_88Q2112_INIT_ERROR_SMI;

    // 2) Init Sequence
    setMasterSlave(phydev, MRVL_88Q2112_INIT_MASTER & flags);
    if (0x0 != (MRVL_88Q2112_INIT_GE & flags)) {
        // manualSetOperateModeSetting supports Z1/A0/A1/A2
        // 1000 BASE-T1: Detect PHY revision, save initial compatibility mode (Default/Legacy), and Initialize.
        if (!manualSetOperateModeSetting(phydev, MRVL_88Q2112_MODE_DEFAULT))
            return MRVL_88Q2112_INIT_ERROR_SAVE_MODE;
        if (!initQ2112Ge(phydev))
            return MRVL_88Q2112_INIT_ERROR_INIT_GE;
    }
    else if (0x0 != (MRVL_88Q2112_INIT_FE & flags)) {
        // 100 BASE-T1 Initialization
        initQ2112Fe(phydev);
    }
    else {
        return MRVL_88Q2112_INIT_ERROR_ARGS;
    }

    // 3) Timeout until link-up and compatibility monitor
    mdelay(10);
    ret = readx_poll_timeout(checkLink, phydev, value,
                             value, 10000, MRVL_88Q2112_LINKUP_TIMEOUT*1000u);
    if(!ret) {
        // link up, exit with success
        pr_info("link up\n");
        return MRVL_88Q2112_INIT_DONE;
    } else {
        isLinkPartnerInLegacyMode(phydev);
    }

    if (MRVL_88Q2112_1000BASE_T1 == getSpeed(phydev)) {
        // 4) 1000 BASE-T1: If link is not established through normal process, here are some possibilities:
        //   a) Local Master/Slave setting is incorrect - rerun function with correct setting
        //   b) Link Partner failed to initialize - system-level problem handled in upper layers
        //   c) Link Partner is Legacy 88Q2112 Z1/A0/A1. Signalled LEGACY in step (3) - restart timer
        //   d) Link Partner is 88Q2112 Z1/A0/A1 revision and did not execute API V1.10 or later version.
        //      Switch mode and restart timer.
        if (!manualSetOperateModeSetting(phydev, MRVL_88Q2112_MODE_LEGACY))
            return MRVL_88Q2112_INIT_ERROR_SAVE_MODE;
        if (!applyOperateModeConfig(phydev))
            return MRVL_88Q2112_INIT_ERROR_SAVE_MODE;

        // There is no need to toggle reset or execute init sequence again.
        ret = readx_poll_timeout(checkLink, phydev, value,
                             value, 10000, MRVL_88Q2112_LINKUP_TIMEOUT*1000u);
        if(!ret) {
            pr_info("link up\n");
            return MRVL_88Q2112_INIT_DONE;
        }
    }
    // Init failed - no link within expected time.
    // 1000 BASE-T1: a loop could be implemented
    // to toggle back into MRVL_88Q2112_MODE_DEFAULT and try again.
    return MRVL_88Q2112_INIT_ERROR_FAILED;
}
#endif // 88Q211x_Z1_A0_A1_A2_API_V1_20

static int m88q2110_ack_interrupt(struct phy_device *phydev)
{
    return 0;
}

static int m88q2110_config_intr(struct phy_device *phydev)
{
    TEST_PRINT("entry");
    return 0;
}

static int m88q2110_read_status(struct phy_device *phydev)
{
    int lpa;
    TEST_PRINT("entry");
    phydev->link = (checkLink(phydev))?1:0;
    phydev->duplex = DUPLEX_FULL;
    phydev->speed = SPEED_100;
    if(MRVL_88Q2112_1000BASE_T1 == getSpeed(phydev)) {
        phydev->speed = SPEED_1000;
    }

    phydev->pause = 0;
    phydev->asym_pause = 0;
    phydev->lp_advertising = 0;

    if (AUTONEG_ENABLE == phydev->autoneg) {
        lpa = regRead(phydev, 7, 0x0205);
        phydev->lp_advertising = mii_adv_to_ethtool_adv_t(lpa & ~(0xFFu << 5)); // NOTE(Toan): clear 10/100 duplex & pause capability due to register mismatching
        if(lpa & BIT(10)) {
            phydev->lp_advertising |= ADVERTISED_Asym_Pause;
            phydev->asym_pause = 1;
        } else {
            phydev->lp_advertising |= ADVERTISED_Pause;
            phydev->pause = 1;
        }
    }

    return 0;

}

static int m88q2110_config_aneg(struct phy_device *phydev)
{
    TEST_PRINT("entry");
    if (AUTONEG_ENABLE != phydev->autoneg) {
        // setup force
        phyInit(phydev, MRVL_88Q2112_INIT_FE | phydev->dev_flags, 0);
        mdelay(5);
        m88q2110_read_status(phydev);
        return 0;
    }

    //NOTE(Toan): Negotiation fails if both sides set force master
    if(!setAneg ( phydev, 0 /*no force master*/ , MRVL_88Q2112_AN_GE_ABILITY )) {
        return -EPFNOSUPPORT;
    }

    return 0;
}

static void m88q2110_get_stats(struct phy_device *phydev,
                  struct ethtool_stats *stats, u64 *data)
{
    TEST_PRINT("entry");
}

static int m88q2110_config_init(struct phy_device *phydev)
{
    int value = 0;
    int ret = 0;
    TEST_PRINT("entry");
    ret = readx_poll_timeout(getModelNum, phydev, value,
                             MRVL_88Q2112_PRODUCT_ID == value, 1000, 100000);
    if (ret)
        return ret;

    phydev->supported &= ~SUPPORTED_1000baseT_Half;
    phydev->advertising &= ~(ADVERTISED_Pause | ADVERTISED_Asym_Pause);

    return 0;
}

/* marvell_suspend
 *
 * Some Marvell's phys have two modes: fiber and copper.
 * Both need to be suspended
 */
static int m88q2110_suspend(struct phy_device *phydev)
{
    TEST_PRINT("entry");
    return 0;
}

/* marvell_resume
 *
 * Some Marvell's phys have two modes: fiber and copper.
 * Both need to be resumed
 */
static int m88q2110_resume(struct phy_device *phydev)
{
    TEST_PRINT("entry");
    return 0;
}

static int m88q2110_aneg_done(struct phy_device *phydev)
{
    TEST_PRINT("entry");
    return (regRead(phydev, 7, 0x0201) & MRVL_88Q2112_AN_COMPLETE);
}

static int m88q2110_did_interrupt(struct phy_device *phydev)
{
    return 0;
}

static int m88q2110_get_sset_count(struct phy_device *phydev)
{
    return 0;
}

// static void m88q2110_get_strings(struct phy_device *phydev, u8 *data)
// {
//     TEST_PRINT("entry");
// }

static int m88q2110_probe(struct phy_device *phydev)
{
    TEST_PRINT("entry");
    return 0;
}

static int m88q2110_soft_reset(struct phy_device *phydev)
{
    TEST_PRINT("entry");
    //_applyQ2112GeSoftReset(phydev);
    return 0;
}
#if 1
//test
// Get real time PMA link status
// @param phydev the phy_device struct
// @return true if link is up, false otherwise
static bool getRealTimeLinkStatus ( struct phy_device *phydev ) {
    uint16_t retData1 = 0;
	retData1 = getSpeed(phydev);
    if (MRVL_88Q2112_1000BASE_T1 == getSpeed(phydev)) {
        retData1 = regRead(phydev, 3, 0x0901);
        retData1 = regRead(phydev, 3, 0x0901);    // Read twice: register latches low value
    }
    else {
        retData1 = regRead(phydev, 3, 0x8109);
    }
    return (0x0 != (retData1 & 0x0004));
}
// Signal Quality Indicator (SQI) with 8 levels (7 is best quality)
// @param phydev the phy_device struct
// @return 8-level quality indicator
static uint16_t getSQIReading_8Level ( struct phy_device *phydev ) {
    uint16_t SQI = 0;
    uint16_t regVal = 0;

    //check link
    if (!getRealTimeLinkStatus(phydev)) {
        return 0;
    }

    // read different register for different rates
    if (MRVL_88Q2112_100BASE_T1 == getSpeed(phydev)) {  // 100 BASE-T1
        regVal = regRead(phydev, 3, 0x8230);
        SQI = (regVal & 0xE000) >> 13;
    }
    else {  // 1000 BASE-T1
        if (MRVL_88Q2112_Z1 != getRevNum(phydev)) {    // A2/A1/A0
            regVal = regRead(phydev, 3, 0xfC4C);
            if (0x0 == (regVal & 0x03FF))
                regVal = 0x03FF;
#if 1 // send the temporary value to userspace app for processing
            SQI = regVal;
#else
            double valDouble = (regVal & 0x03FF) / pow(2.0, 10);
            if (valDouble > 0.35) valDouble = 0.35;
            if (valDouble < 8e-3) valDouble = 8e-3;

            double test = 4.657790851448008 * pow(valDouble, 6)
                - 5.602322633777736 * pow(valDouble, 5)
                + 2.659063584758740 * pow(valDouble, 4)
                - 0.637339073242494 * pow(valDouble, 3)
                + 0.083157751272959 * pow(valDouble, 2)
                - 0.006409214385559 * valDouble + 0.000394024510294;
            test = test*1e5;

            if (test>26.93) {
                SQI = 7;
            }
            else if (test>24.04) {
                SQI = 6;
            }
            else if (test>22.41) {
                SQI = 5;
            }
            else if (test>21.06) {
                SQI = 4;
            }
            else if (test>19.69) {
                SQI = 3;
            }
            else if (test>18.88) {
                SQI = 2;
            }
            else if (test>17.88) {
                SQI = 1;
            }
            else {
                SQI = 0;
            }
#endif
        }
        else {  // Z1 Case
            uint16_t trigVal = 0;
            regWrite(phydev, 3, 0xFC5D, 0x06BF);

            //request SQI value
            trigVal = regRead(phydev, 3, 0xFC89);
            regVal = trigVal | 0x1;
            regWrite(phydev, 3, 0xFC89, regVal);
            mdelay(5); //delay 5ms

            //read SQI value
            regWrite(phydev, 3, 0xFC5D, 0x86BF);
            regVal = regRead(phydev, 3, 0xFC88);
            SQI = (regVal & 0xE) >> 1;

            // release the request
            regWrite(phydev, 3, 0xFC5D, 0x06BF);
            regWrite(phydev, 3, 0xFC89, trigVal);
        }
    }
    return SQI;
}

// Signal Quality Indicator (SQI) with 16 levels (15 is best quality)
// @param phydev the phy_device struct
// @return quality indicator
static uint16_t getSQIReading_16Level ( struct phy_device *phydev ) {
    uint16_t SQI = 0;
    uint16_t regVal = 0;

    //check link
    if (!getRealTimeLinkStatus(phydev))
        return 0;

    // read different register for different rates
    if (MRVL_88Q2112_100BASE_T1 == getSpeed(phydev)) {  // 100 BASE-T1
        regVal = regRead(phydev, 3, 0x8230);
        SQI = (regVal & 0xF000) >> 12;
    }
    else {  // 1000 BASE-T1
        if (MRVL_88Q2112_Z1 != getRevNum(phydev)) {    // A2/A1/A0
            regVal = regRead(phydev, 3, 0xfC4C);
            if ((regVal & 0x03FF) == 0)
                regVal = 0x03FF;
#if 1 // send the temporary value to userspace app for processing
            SQI = regVal;
#else
            double valDouble = (regVal & 0x03FF) / pow(2.0, 10);
            if (valDouble > 0.35) valDouble = 0.35;
            if (valDouble < 8e-3) valDouble = 8e-3;

            double test = 4.657790851448008 * pow(valDouble, 6)
                - 5.602322633777736 * pow(valDouble, 5)
                + 2.659063584758740 * pow(valDouble, 4)
                - 0.637339073242494 * pow(valDouble, 3)
                + 0.083157751272959 * pow(valDouble, 2)
                - 0.006409214385559 * valDouble + 0.000394024510294;
            test = test*1e5;

            if (test>28.2) {
                SQI = 15;
            }
            else if (test>26.93) {
                SQI = 14;
            }
            else if (test>25.51) {
                SQI = 13;
            }
            else if (test>24.04) {
                SQI = 12;
            }
            else if (test>23.39) {
                SQI = 11;
            }
            else if (test>22.41) {
                SQI = 10;
            }
            else if (test>21.70) {
                SQI = 9;
            }
            else if (test>21.06) {
                SQI = 8;
            }
            else if (test>20.30) {
                SQI = 7;
            }
            else if (test>19.69) {
                SQI = 6;
            }
            else if (test>19.33) {
                SQI = 5;
            }
            else if (test>18.88) {
                SQI = 4;
            }
            else if (test>18.46) {
                SQI = 3;
            }
            else if (test>17.88) {
                SQI = 2;
            }
            else if (test>17.43) {
                SQI = 1;
            }
            else {
                SQI = 0;
            }
#endif
        }
        else {  // Z1 Case
            uint16_t trigVal = 0;
            regWrite(phydev, 3, 0xFC5D, 0x06BF);

            //request SQI value
            trigVal = regRead(phydev, 3, 0xFC89);
            regVal = trigVal | 0x1;
            regWrite(phydev, 3, 0xFC89, regVal);
            mdelay(5); //delay 5ms

            //read SQI value
            regWrite(phydev, 3, 0xFC5D, 0x86BF);
            regVal = regRead(phydev, 3, 0xFC88);
            SQI = regVal & 0xF;

            // release the request
            regWrite(phydev, 3, 0xFC5D, 0x06BF);
            regWrite(phydev, 3, 0xFC89, trigVal);
        }
    }
    return SQI;
}

// Cable Quality Indicator (CQI) providing Insertion Loss (IL) and Return Loss (RL)
// Valid for A2/A1/A0 only, not Z1
// @param phydev the phy_device struct
// @return true when measurement complete, false if link down or on error
static uint32_t getCQIReading ( struct phy_device *phydev) {
    uint16_t regVal = 0;
    uint16_t rb_orig = 0;
    uint16_t IL, RL;
    uint32_t ret = 0;
    int cnt = 0;

    // Only for A2/A1/A0
    if (MRVL_88Q2112_Z1 == getRevNum(phydev))
        return ret;

    //check link
    if (!getRealTimeLinkStatus(phydev))
        return ret;

    // procedure differs between data rates
    if (MRVL_88Q2112_100BASE_T1 == getSpeed(phydev)) {  // 100 BASE-T1
        
        int16_t cqi_il_offset = 16;
        int16_t cqi_rl_offset = -29;

        rb_orig = regRead(phydev, 3, 0xfb80);
        regWrite(phydev, 3, 0xfb80, rb_orig & 0xfff7);

        regVal = (regRead(phydev, 3, 0xfbee) & 0xfff0) | 0x000a;
        regWrite(phydev, 3, 0xfbee, regVal);
        regWrite(phydev, 3, 0xfbe8, 0xd08e);
        regWrite(phydev, 3, 0xfbe9, 0x3001);
        regWrite(phydev, 3, 0xfbea, 0x774f);
        regWrite(phydev, 3, 0xfbeb, 0x1040);

        cqi_il_offset = cqi_il_offset * 2;
        if (cqi_il_offset<0) {
            cqi_il_offset = cqi_il_offset + 256;
        }
        regWrite(phydev, 3, 0xfbec, 64 + cqi_il_offset * 256);
        regWrite(phydev, 3, 0xfbed, 0x783c);

        regVal = (regRead(phydev, 3, 0xfbef) & 0xFF00) | 0x0018;
        regWrite(phydev, 3, 0xfbef, regVal);

        cqi_rl_offset = cqi_rl_offset * 2;
        if (cqi_rl_offset<0) {
            cqi_rl_offset = cqi_rl_offset + 256;
        }
        regWrite(phydev, 3, 0xfbf0, 64 + cqi_rl_offset * 256);

        
        regVal = regRead(phydev, 3, 0xfbee) | 0x0030;
        regWrite(phydev, 3, 0xfbee, regVal);
        regWrite(phydev, 3, 0xfbee, regVal & 0xffcf);
        regWrite(phydev, 3, 0xfbee, regVal);
        
        regVal = regRead(phydev, 3, 0xfb80);
        regWrite(phydev, 3, 0xfb80, regVal | 0x0008);
        
        regVal = regRead(phydev, 3, 0xfba1);
        regWrite(phydev, 3, 0xfba1, regVal | 0x0001);
        regVal = regRead(phydev, 3, 0xfb94);
        regWrite(phydev, 3, 0xfb94, regVal | 0x0001);

        while (true) {
            regVal = regRead(phydev, 3, 0xfbf4);
            if (0x0 == (regVal & 0x3000))
                break;
            mdelay(10);
            cnt++;
            if (cnt > 100)
                break;
        }

        
        regVal = regRead(phydev, 3, 0xfbf4);
        IL = (regVal & 0x00f0) >> 4;
        RL = (regVal & 0x0f00) >> 8;

        
        regVal = regRead(phydev, 3, 0xfba1);
        regWrite(phydev, 3, 0xfba1, regVal & 0xfffe);

        regVal = regRead(phydev, 3, 0xfb94);
        regWrite(phydev, 3, 0xfb94, regVal & 0xfffe);

        regVal = regRead(phydev, 3, 0xfbee);
        regWrite(phydev, 3, 0xfbee, regVal & 0xfffe);  

        
        regWrite(phydev, 3, 0xfb80, rb_orig);
    }
    else {  // 1000 BASE-T1
        
        int16_t cqi_il_offset = 31;
        int16_t cqi_rl_offset = -26;

        rb_orig = regRead(phydev, 3, 0xfc00);
        regWrite(phydev, 3, 0xfc00, rb_orig & 0xfff7);

        cqi_il_offset = cqi_il_offset * 2;
        if (cqi_il_offset<0) {
            cqi_il_offset = cqi_il_offset + 256;
        }
        regWrite(phydev, 3, 0xfc64, 63 + cqi_il_offset * 256);

        cqi_rl_offset = cqi_rl_offset * 4;
        if (cqi_rl_offset<0) {
            cqi_rl_offset = cqi_rl_offset + 256;
        }
        regWrite(phydev, 3, 0xfc7c, 64 + cqi_rl_offset * 256);

        
        regVal = regRead(phydev, 3, 0xfc16);
        regWrite(phydev, 3, 0xfc16, regVal | 0x0001);

        
        regVal = regRead(phydev, 3, 0xfc89);
        regWrite(phydev, 3, 0xfc89, regVal & 0xFFED);
        regWrite(phydev, 3, 0xfc89, regVal | 0x0012);

        mdelay(500);

        
        regVal = regRead(phydev, 3, 0xfc00);
        regWrite(phydev, 3, 0xfc00, regVal | 0x0008);

        
        regVal = regRead(phydev, 3, 0xfc66);
        IL = (regVal & 0xf000) >> 12;
        regVal = regRead(phydev, 3, 0xfc80);
        RL = regVal / 64;

        if (IL > 15) IL = 15;
        if (RL > 15) RL = 15;

        
        regVal = regRead(phydev, 3, 0xfc16);
        regWrite(phydev, 3, 0xfc16, regVal & 0xFFFE);

        
        regWrite(phydev, 3, 0xfc00, rb_orig);
        pr_warn("%s IL=%u, RL=%u\n", __func__, IL, RL);
    }
    ret = (IL << 16) | RL;
    return ret;
}

// Virtual Cable Test (VCT) intended to find faults in cables and improper termination
// @param phydev the phy_device struct
// @param &distanceToFault if test successful, distance to fault will be stored here
// @param &cable_status if test successful, cable status will be stored here
// @return true when measurement complete, false if test failed
//     In case method returns true, interpret results as follows:
//     cable_status     distanceToFault     result
//     't'              0                   proper termination
//     'o'              value               open circuit at length "value"
//     's'              value               short circuit at length "value"
//     No other results possible
static uint32_t getVCTReading ( struct phy_device *phydev) {
    uint16_t reg_fec9 = 0;
    uint16_t retData = 0;
    uint16_t cnt = 0;
    uint16_t vct_dis = 0;
    uint16_t vct_amp = 0;
    uint16_t vct_polarity = 0;
    uint32_t ret = 0;

    // Ignore Wire Activity
    reg_fec9 = regRead(phydev, 3, 0xfec9);
    regWrite(phydev, 3, 0xfec9, reg_fec9 | 0x0080);

    // Set incoming ADC sign bit
    retData = regRead(phydev, 3, 0xfec3);
    regWrite(phydev, 3, 0xfec3, retData | 0x2000);

    retData = regRead(phydev, 3, 0xfe5d);
    regWrite(phydev, 3, 0xfe5d, retData & 0xEFFF);

    // Adjust thresholds
    regWrite(phydev, 3, 0xfec4, 0x0f1f);
    regWrite(phydev, 3, 0xfec7, 0x1115);

    // Enable TDR (self-clear register)
    retData = regRead(phydev, 3, 0xfec3);
    regWrite(phydev, 3, 0xfec3, retData | 0x4000);
    mdelay(210); //delay 210ms

    // Check if test is done
    while (true) {
        //make sure previous VCT is done  
        retData = regRead(phydev, 3, 0xFEDB);
        if (0x0 == (retData & 0x0400))   //done = 0x0400, busy = 0
            mdelay(20); //delay 20ms
        else
            break;  // test done
        cnt++;
        if (cnt > 20) { // Timeout error condition
            regWrite(phydev, 3, 0xfec9, reg_fec9);
            return false;
        }
    }

    // Capture results
    retData = regRead(phydev, 3, 0xfedb);
    vct_dis = retData & 0x03ff;
    retData = regRead(phydev, 3, 0xfedc);
    vct_amp = retData & 0x007f;
    vct_polarity = retData & 0x0080;
    
#if 1 // send the temporary value to userspace app for processing
    ret = (vct_polarity << 16) | vct_dis;
#else
    // process results
    if (0x0 == vct_dis) {
        distanceToFault = 0;
    }
    else {
        distanceToFault = (vct_dis - 39) * 0.12931;
    }
    if ((0x0 == vct_dis) && (0x0 != vct_polarity)) {
        cable_status = 't';
    }
    else if (0x0 != vct_polarity) {
        cable_status = 'o';
    }
    else {
        cable_status = 's';
    }
#endif
    // stop test
    regWrite(phydev, 3, 0xfec9, reg_fec9);
    return ret;
}

static void m88q2110_test(struct phy_device *dev, u8 *buf)
{
    int ret = 0;
    uint32_t *code = (uint32_t*)buf;
    pr_warn("%s entry\n", __func__);
    switch (code[0])
    {
    case 1:
        ret = getSQIReading_8Level(dev);
        break;
    case 2:
        ret = getSQIReading_16Level(dev);
        break;
    case 3:
        ret = getVCTReading(dev);
        break;
    case 4:
        ret = getCQIReading(dev);
        break;
    default:
        break;
    }

    code[1] = ret;
}
#endif
//test

// Enable Auto-Negotiation
// @param phydev the phy_device struct
// @param forceMaster non-zero: force local PHY to be master
// @param flags supported rates MRVL_88Q2112_AN_XXXX   (see 3.1 Definitions)
// @return true if successfully initialized, false on error
//  Example: setAneg(phydev, 0, MRVL_88Q2112_AN_GE_ABILITY | MRVL_88Q2112_AN_FE_ABILITY)
static bool setAneg ( struct phy_device *phydev, uint16_t forceMaster, uint16_t flags )
{
    uint16_t regDataAuto, anAbility = 0;

    pr_warn("setting auto-negotiation\n");

    if (0x0 != forceMaster)
        anAbility = MRVL_88Q2112_AN_PREFER_MASTER;

    // Disable AN if no data rates supported
    if (0x0 == (flags & (MRVL_88Q2112_AN_GE_ABILITY | MRVL_88Q2112_AN_FE_ABILITY))) {
        regWrite(phydev, 7, 0x0203, MRVL_88Q2112_AN_DISABLE);
        regWrite(phydev, 7, 0x0202, 0x0001);
        regWrite(phydev, 7, 0x0200, MRVL_88Q2112_AN_RESET);
        return true;
    }

    // Initialize for 1 or more data rates
    if (0x0 != (flags & MRVL_88Q2112_AN_GE_ABILITY)) {
        _applyQ2112GeSetting(phydev, MRVL_88Q2112_AN_ENABLE);
        if (!applyOperateModeConfig(phydev))
            return false;
        anAbility |= MRVL_88Q2112_AN_GE_ABILITY;
    }

    if (0x0 != (flags & MRVL_88Q2112_AN_FE_ABILITY)) {
        _applyQ2112FeSetting(phydev, MRVL_88Q2112_AN_ENABLE);
        anAbility |= MRVL_88Q2112_AN_FE_ABILITY;
    }

    _applyQ2112AnegSetting(phydev);

    // Set Ability registers
    regWrite(phydev, 7, 0x0203, anAbility);

    // Force master if needed
    regDataAuto = regRead(phydev, 7, 0x0202);
    if (0x0 != forceMaster)
        regDataAuto |= 0x1000;
    else
        regDataAuto &= 0xEFFF;
    regWrite(phydev, 7, 0x0202, regDataAuto);

    // Reset routines to restart Auto-Negotiation
    if (0x0 != (flags & MRVL_88Q2112_AN_GE_ABILITY))
        _applyQ2112GeSoftReset(phydev);
    if (0x0 != (flags & MRVL_88Q2112_AN_FE_ABILITY))
        _applyQ2112FeSoftReset(phydev);
    return true;
}

static struct phy_driver marvell_88q2110_drivers[] = {
    {
        .phy_id = 0x01410c00,
        .phy_id_mask = MARVELL_PHY_ID_MASK,
        .name = "Marvell 88Q2110",
        .features = PHY_DEFAULT_FEATURES | SUPPORTED_1000baseT_Full,
        .probe = m88q2110_probe,
        .flags = PHY_HAS_INTERRUPT,
        .soft_reset = m88q2110_soft_reset,
        .config_init = m88q2110_config_init,
        .config_aneg = m88q2110_config_aneg,
        .read_status = m88q2110_read_status,
        .ack_interrupt = m88q2110_ack_interrupt,
        .config_intr = m88q2110_config_intr,
        .resume = m88q2110_resume,
        .suspend = m88q2110_suspend,
        .get_sset_count = m88q2110_get_sset_count,
        //.get_strings = m88q2110_get_strings,
        .get_stats = m88q2110_get_stats,
        .aneg_done = m88q2110_aneg_done,
        .did_interrupt = m88q2110_did_interrupt,
        .get_strings = m88q2110_test,
    },
};

module_phy_driver(marvell_88q2110_drivers);

static struct mdio_device_id __maybe_unused marvell_88q2110_tbl[] = {
    { 0x01410c00, MARVELL_PHY_ID_MASK },
    { }
};

MODULE_DEVICE_TABLE(mdio, marvell_88q2110_tbl);
